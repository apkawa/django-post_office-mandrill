# -*- coding: utf-8 -*-

from post_office.models import Email

from django.core import mail
from django.test import TestCase

from post_office_mandrill.models import STATUS


class ModelTest(TestCase):

    def test_dispatch(self):
        """
        Ensure that email.dispatch() actually sends out the email
        """
        email = Email.objects.create(to=['to@example.com'], from_email='from@example.com',
                                     subject='Test dispatch', message='Message', backend_alias='locmem')
        email.dispatch()
        self.assertEqual(mail.outbox[0].subject, 'Test dispatch')

    def test_mandrill_dispatch(self):
        """
        Ensure that email.dispatch() actually sends out the email
        """
        email = Email.objects.create(to=['to@example.com'], from_email='from@example.com',
            subject='Test dispatch', message='Message', backend_alias='post_office_mandrill')
        email.dispatch()
        self.assertEqual(email.mandrill_logs.count(), 1)
        mandrill_log = email.mandrill_logs.all()[0]
        self.assertEqual(mandrill_log.email, email)
        self.assertEqual(mandrill_log.status, STATUS.sent)

