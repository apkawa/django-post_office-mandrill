# -*- coding: utf-8 -*-

from post_office.models import Email

from . import TestCase



class ModelTest(TestCase):

    def test_fetch_info(self):
        """
        Ensure that email.dispatch() actually sends out the email
        """
        email = Email.objects.create(to=['to@example.com'], from_email='from@example.com',
            subject='Test dispatch live', message='Message', backend_alias='post_office_mandrill')
        email.dispatch()
        self.assertEqual(email.mandrill_logs.count(), 1)
        mandrill_log = email.mandrill_logs.all()[0]
        info = mandrill_log.fetch_info()
        self.assertTrue(info)

