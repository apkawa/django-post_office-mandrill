# -*- coding: utf-8 -*-
import unittest

from django.test import TestCase


# Workaround for pycharm
class TestCase(type("TestCase", (TestCase, ), {}), unittest.TestCase):
    pass
