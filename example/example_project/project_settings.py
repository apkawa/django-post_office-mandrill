# -*- coding: utf-8 -*-

EMAIL_BACKEND = 'post_office.EmailBackend'

POST_OFFICE = {
    'BACKENDS': {
        'default': 'post_office_mandrill.backends.PostOfficeMandrillBackend',
    }
}

MANDRILL_API_KEY = 'PUT-YOUR-MANDRILL-API-KEY'

try:
    from .local_settings import *
except ImportError:
    pass
