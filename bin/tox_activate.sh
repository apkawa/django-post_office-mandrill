#!/bin/bash
SCRIPT_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}")" && pwd)

TOX_DIR=$SCRIPT_DIR/../.tox
ENV_NAME=py34-django19

if [ $1 ]
then
    ENV_NAME=$1
fi

. $TOX_DIR/$ENV_NAME/bin/activate


