# -*- coding: utf-8 -*-

from django.conf.urls import url, include
import djrill.urls

urlpatterns = ('',
               url(r'^djrill/', include(djrill.urls)),
               )
