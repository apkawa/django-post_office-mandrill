# -*- coding: utf-8 -*-
from collections import namedtuple
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from django.db import models
from jsonfield import JSONField
from post_office.compat import text_type

STATUS_VALUES = 'sent queued scheduled rejected invalid'
STATUS = namedtuple('STATUS', STATUS_VALUES)._make(STATUS_VALUES.split(' '))

REJECT_REASON_VALUES = 'hard-bounce soft-bounce spam unsub custom invalid-sender invalid test-mode-limit rule'
REJECT_REASON = namedtuple('STATUS', REJECT_REASON_VALUES.replace('-', '_'))._make(REJECT_REASON_VALUES.split(' '))


class MandrillLog(models.Model):
    """
    A model to record sending email sending activities.
    """

    STATUS_CHOICES = STATUS.__dict__.items()
    REJECT_REASON_CHOICES = REJECT_REASON.__dict__.items()

    email = models.ForeignKey('post_office.Email', editable=False, related_name='mandrill_logs')
    date = models.DateTimeField(auto_now_add=True)


    response = JSONField()
    message_id = models.CharField(max_length=128, blank=True)

    status = models.CharField(max_length=25, choices=STATUS_CHOICES)
    reject_reason = models.CharField(max_length=25, choices=REJECT_REASON_CHOICES, blank=True, null=True)

    bounce_description = models.TextField(blank=True)

    clicks = models.PositiveIntegerField(default=0)
    opens = models.PositiveIntegerField(default=0)

    class Meta:
        app_label = 'post_office_mandrill'

    def __unicode__(self):
        return "%s %s" % (text_type(self.date), self.message_id)

    def fetch_info(self):
        import mandrill
        m = mandrill.Mandrill(settings.MANDRILL_API_KEY)
        return m.messages.info(self.message_id)

    def update_info(self):
        info = self.fetch_info()
        for field in ['clicks', 'opens', 'status', 'reject_reason']:
            value = info.get(field)
            if value:
                setattr(self, field, value)

