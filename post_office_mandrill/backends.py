# -*- coding: utf-8 -*-
import logging
from djrill.mail.backends.djrill import DjrillBackend
from post_office.models import Email

logger = logging.getLogger(__name__)


class PostOfficeMandrillBackend(DjrillBackend):
    def _send(self, email):
        status = super(PostOfficeMandrillBackend, self)._send(email)

        if status:
            post_office_email = getattr(email, 'post_office_email', None)
            if not post_office_email and isinstance(email, Email):
                post_office_email = email

            mandril_response = email.mandrill_response[0]
            post_office_email.mandrill_logs.create(
                response=mandril_response,
                message_id=mandril_response['_id'],
                status=mandril_response['status'],
                reject_reason=mandril_response.get('reject_reason')
            )
