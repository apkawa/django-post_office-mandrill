# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('post_office', '0002_add_i18n_and_backend_alias'),
    ]

    operations = [
        migrations.CreateModel(
            name='MandrillLog',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('response', jsonfield.fields.JSONField()),
                ('message_id', models.CharField(max_length=128, blank=True)),
                ('status', models.CharField(choices=[('sent', 'sent'), ('queued', 'queued'), ('scheduled', 'scheduled'), ('rejected', 'rejected'), ('invalid', 'invalid')], max_length=25)),
                ('reject_reason', models.CharField(choices=[('hard_bounce', 'hard-bounce'), ('soft_bounce', 'soft-bounce'), ('spam', 'spam'), ('unsub', 'unsub'), ('custom', 'custom'), ('invalid_sender', 'invalid-sender'), ('invalid', 'invalid'), ('test_mode_limit', 'test-mode-limit'), ('rule', 'rule')], max_length=25, blank=True, null=True)),
                ('bounce_description', models.TextField(blank=True)),
                ('clicks', models.PositiveIntegerField(default=0)),
                ('opens', models.PositiveIntegerField(default=0)),
                ('email', models.ForeignKey(related_name='mandrill_logs', editable=False, to='post_office.Email')),
            ],
        ),
    ]
