# -*- coding: utf-8 -*-
from djrill.signals import webhook_event
from django.dispatch import receiver

from .models import MandrillLog

EVENT_TYPE_TO_FIELD_MAP = {
    'click': 'clicks',
    'view': 'views'
}


# TODO move to settings like as MANDRILL_WEBHOOKS_HANDLERS = {'*': ['package.handle_stats'], 'click': ['package.handle_click']}

@receiver(webhook_event)
def handle_stats(sender, event_type, data, **kwargs):
    if event_type in ['click', 'view']:
        message_id = data['msg']['_id']
        field_name = EVENT_TYPE_TO_FIELD_MAP[event_type]
        MandrillLog.objects.filter(message_id=message_id).update(**{field_name: data['msg'][field_name]})


@receiver(webhook_event)
def handle_bounce(sender, event_type, data, **kwargs):
    if event_type in ['hard_bounce', 'soft_bounce', 'spam', 'reject']:
        message_id = data['msg']['_id']
        MandrillLog.objects.filter(message_id=message_id).update(
            status='rejected',
            reject_reason=data['msg']['state'],
            bounce_description=data['msg']['bounce_description'],
        )




