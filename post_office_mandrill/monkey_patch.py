# -*- coding: utf-8 -*-

import post_office.models

old_email_message = post_office.models.Email.email_message

def new_email_message(self, connection=None):
    msg = old_email_message(self, connection)
    msg.post_office_email = self
    return msg


def do_path():
    post_office.models.Email.add_to_class('email_message', new_email_message)

